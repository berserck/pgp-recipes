
async function populate() {

    const urlParams = new URLSearchParams(window.location.search);
    const myParam = urlParams.get('Recipe');

    const requestURL = `data/${myParam}.json`;
    const request = new Request(requestURL);

    const response = await fetch(request);
    const recipesText = await response.json();

    populateRecipe(recipesText);
}

function populateRecipe(recipe) {
    const html = document.querySelector('html');
    html.lang = recipe.lang;

    const title = document.querySelector('title');
    title.textContent = recipe.title;

    populateMetas(recipe);

    const description = document.querySelector("meta[name='description']");
    description.content = recipe.description
    const author = document.querySelector("meta[name='author']");
    author.content = recipe.author
    const current_recipe = document.querySelector('li.active');
    current_recipe.textContent = recipe.title

    const header_title = document.querySelector('h1.recipe-name');
    header_title.textContent = recipe.title

    populateIngredientsSection(recipe);

    populateInstructionsSection(recipe);

    if (recipe.hasOwnProperty("origin")) {
        const container = document.querySelector('div.container');
        const originLink = document.createElement('a');
        originLink.href = recipe.origin.href;
        originLink.textContent = recipe.origin.text;
        container.appendChild(originLink);
    }
}

populate();

function populateInstructionsSection(recipe) {
    const instructions_section = document.querySelector('div.instructions');
    const instructions_lists = recipe.instructions_lists;
    for (const instructions_list of instructions_lists) {
        const instructions_title = document.createElement('h2');
        instructions_title.textContent = instructions_list.title;
        const instructions_steps = instructions_list.instructions;
        const instructions_ol = document.createElement('ol');

        for (const instructions_item of instructions_steps) {
            const instructions_li = document.createElement('li');
            instructions_li.innerHTML = instructions_item;
            instructions_ol.appendChild(instructions_li);
        }
        instructions_section.appendChild(instructions_title);
        instructions_section.appendChild(instructions_ol);
    }
}

function populateIngredientsSection(recipe) {
    const ingredients_section = document.querySelector('div.ingredients');
    const ingredients_lists = recipe.ingredients_lists;
    for (const ingredients_list of ingredients_lists) {
        const ingredients_title = document.createElement('h2');
        ingredients_title.textContent = ingredients_list.title;
        const ingredients_items = ingredients_list.ingredients;
        const ingredients_ul = document.createElement('ul');

        for (const ingredients_item of ingredients_items) {
            const ingredients_li = document.createElement('li');
            ingredients_li.innerHTML = ingredients_item;
            ingredients_ul.appendChild(ingredients_li);
        }
        ingredients_section.appendChild(ingredients_title);
        ingredients_section.appendChild(ingredients_ul);
    }
}

function populateMetas(recipe) {
    if (!recipe.hasOwnProperty("metas")) {
        return;
    }
    const metas = recipe.metas;
    const head = document.querySelector('head');
    for (const metaItem of metas) {
        const meta = document.createElement('meta');
        meta.name = metaItem.name;
        meta.content = metaItem.value;
        head.appendChild(meta);
    }
}
