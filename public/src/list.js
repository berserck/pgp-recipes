
async function populate() {

    const requestURL = 'data/list.json';
    const request = new Request(requestURL);

    const response = await fetch(request);
    const recipesText = await response.json();

    populateRecipes(recipesText);
}

function populateRecipes(obj){
    const section = document.querySelector('.list');
    const recipes = obj.recipes.sort((a, b) => a.name.localeCompare(b.name));
    for (const recipe of recipes) {
        const myRecipeLink = document.createElement('a');
        myRecipeLink.href = recipe.href;
        const recipeTitle = document.createElement('p');
        recipeTitle.textContent = recipe.name;
        myRecipeLink.appendChild(recipeTitle);
        section.appendChild(myRecipeLink);
    }
}

populate();